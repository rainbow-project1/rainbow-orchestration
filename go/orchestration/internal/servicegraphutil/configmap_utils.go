package servicegraphutil

import (
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	fogappsCRDs "k8s.rainbow-h2020.eu/rainbow/orchestration/apis/fogapps/v1"
)

// CreateConfigMap creates a new K8s ConfigMap, based on a ConfigMap from a ServiceGraph.
func CreateConfigMap(configMapName string, rainbowConfigMap *fogappsCRDs.ConfigMap, graph *fogappsCRDs.ServiceGraph) (*core.ConfigMap, error) {
	configMap := &core.ConfigMap{
		ObjectMeta: meta.ObjectMeta{
			Name:      configMapName,
			Namespace: graph.Namespace,
		},
	}
	return UpdateConfigMap(configMap, rainbowConfigMap, graph)
}

// UpdateConfigMap updates an existing K8s ConfigMap, based on a ConfigMap from a ServiceGraph.
func UpdateConfigMap(existingConfigMap *core.ConfigMap, rainbowConfigMap *fogappsCRDs.ConfigMap, graph *fogappsCRDs.ServiceGraph) (*core.ConfigMap, error) {
	rainbowConfigMapCopy := rainbowConfigMap.DeepCopy()
	existingConfigMap.Immutable = &rainbowConfigMapCopy.Immutable
	existingConfigMap.Data = rainbowConfigMapCopy.Data
	existingConfigMap.BinaryData = rainbowConfigMap.BinaryData
	return existingConfigMap, nil
}
