# Rainbow Elasticity Components

This project was generated using [Polaris CLI](https://polaris-slo-cloud.github.io) and [Nx](https://nx.dev).

The elasticity components leverage the [Polaris SLO Cloud framework](https://polaris-slo-cloud.github.io), which is licensed under the Apache 2.0 license.

