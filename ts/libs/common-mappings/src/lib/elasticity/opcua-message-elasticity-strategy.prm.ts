import { ElasticityStrategy, ElasticityStrategyKind, SloCompliance, SloTarget, initSelf } from '@polaris-sloc/core';

/**
 * Configuration options for OpcuaMessageElasticityStrategy.
 */
export interface OpcuaMessageElasticityStrategyConfig {

    /**
     * The URL of the OPC UA server to connect to.
     */
    serverUrl: string;

    /**
     * The name of the OPC UA node to write to.
     */
    nodeName: string;

    /**
     * This is applied when the SLO Compliance is below `100 - tolerance`.
     *
     * If this is not set, no message will be sent when the SLO changes to this condition.
     */
    baseValue?: number;

    /**
     * This is applied when the SLO Compliance is above `100 + tolerance`.
     *
     * If this is not set, no message will be sent when the SLO changes to this condition.
     */
    alternateValue?: number;

}

/**
 * Denotes the elasticity strategy kind for the OpcuaMessageElasticityStrategy.
 */
export class OpcuaMessageElasticityStrategyKind extends ElasticityStrategyKind<SloCompliance, SloTarget> {
    constructor() {
        super({
            group: 'elasticity.k8s.rainbow-h2020.eu',
            version: 'v1',
            kind: 'OpcuaMessageElasticityStrategy',
        });
    }
}

/**
 * Defines the OpcuaMessageElasticityStrategy.
 */
export class OpcuaMessageElasticityStrategy extends ElasticityStrategy<SloCompliance, SloTarget, OpcuaMessageElasticityStrategyConfig> {
    constructor(initData?: Partial<OpcuaMessageElasticityStrategy>) {
        super(initData);
        this.objectKind = new OpcuaMessageElasticityStrategyKind();
        initSelf(this, initData);
    }
}
