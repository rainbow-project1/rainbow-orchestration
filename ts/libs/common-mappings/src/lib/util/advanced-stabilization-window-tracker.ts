import {
    DefaultElasticityStrategyExecutionTracker,
    ElasticityStrategy,
    ElasticityStrategyExecution,
    ElasticityStrategyExecutionTracker,
    StabilizationWindow,
    StabilizationWindowTracker,
} from '@polaris-sloc/core';

/**
 * `StabilizationWindowTracker` implementation that also tracks the content of the applied elasticity strategies.
 *
 * @param E The type of `ElasticityStrategy` for which to track the stabilization windows.
 * @param O The type of operation performed by the elasticity strategy.
 */
export class AdvancedStabilizationWindowTracker<E extends ElasticityStrategy<any>, O> implements StabilizationWindowTracker<E> {

    private executionTracker: ElasticityStrategyExecutionTracker<E, O> = new DefaultElasticityStrategyExecutionTracker();

    /**
     * Gets the last recorded `ElasticityStrategyExecution` for the specified `target`.
     *
     * @param elasticityStrategy The `ElasticityStrategy` instance, for which the execution should be retrieved.
     * @returns The last recorded `ElasticityStrategyExecution` for the specified `elasticityStrategy` (or any of its previous instances) or `undefined`,
     * if no such record exists (i.e., the elasticity strategy has never executed during the lifetime of the controller
     * or the last execution record has expired).
     */
    getLastExecution(elasticityStrategy: E): ElasticityStrategyExecution<E, O> {
        return this.executionTracker.getLastExecution(elasticityStrategy);
    }

    trackExecution(elasticityStrategy: E, operation: O = undefined): void {
        this.executionTracker.addExecution(elasticityStrategy, operation);
    }

    isOutsideStabilizationWindowForScaleUp(elasticityStrategy: E): boolean {
        const stabilizationWindow = StabilizationWindow.getScaleUpSecondsOrDefault(elasticityStrategy.spec.stabilizationWindow);
        return this.checkIfOutsideStabilizationWindow(elasticityStrategy, stabilizationWindow);
    }

    isOutsideStabilizationWindowForScaleDown(elasticityStrategy: E): boolean {
        const stabilizationWindow = StabilizationWindow.getScaleDownSecondsOrDefault(elasticityStrategy.spec.stabilizationWindow);
        return this.checkIfOutsideStabilizationWindow(elasticityStrategy, stabilizationWindow);
    }

    removeElasticityStrategy(elasticityStrategy: E): void {
        this.executionTracker.removeElasticityStrategy(elasticityStrategy);
    }

    evictExpiredExecutions(): void {
        this.executionTracker.evictExpiredExecutions();
    }

    private checkIfOutsideStabilizationWindow(elasticityStrategy: E, stabilizationWindowSec: number): boolean {
        const lastExecution = this.executionTracker.getLastExecution(elasticityStrategy);
        if (!lastExecution) {
            return true;
        }

        const timeDiffSec = (new Date().valueOf() - lastExecution.timestamp.valueOf()) / 1000;
        return timeDiffSec > stabilizationWindowSec;
    }

}
