# @rainbow-h2020/common-mappings

This library contains common SLO mappings from the RAINBOW project.

## Running unit tests

Run `nx test common-mappings` to execute the unit tests via [Jest](https://jestjs.io).
