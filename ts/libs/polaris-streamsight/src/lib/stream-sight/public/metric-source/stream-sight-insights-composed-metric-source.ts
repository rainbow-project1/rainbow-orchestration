import {
    ComposedMetricSourceBase,
    DataType,
    LabelFilters,
    MetricsSource,
    OrchestratorGateway,
    Sample,
    TimeInstantQuery,
    TimeSeriesInstant,
    TimeSeriesSource,
} from '@polaris-sloc/core';
import { Observable } from 'rxjs';
import { filter, finalize, map, share, switchMap, tap } from 'rxjs/operators';
import { PolarisStreamSightConfig } from '../../../config';
import { RainbowStorageTimeSeriesSource } from '../../../rainbow-storage';
import { InsightTopologyManager } from '../../internal/insight-topology-manager';
import { StreamSightInsights, StreamSightInsightsMetric, StreamSightInsightsParams } from '../common';

/**
 * Specifies after how many value requests, the metric mapping should be checked again.
 *
 * See `StreamSightInsightsComposedMetricSource.maintainInsightTopology()`.
 */
const CHECK_INSIGHT_TOPOLOGY_INTERVAL = 100;

/**
 * A {@link ComposedMetricSource} that fetches insights from StreamSight.
 */
export class StreamSightInsightsComposedMetricSource extends ComposedMetricSourceBase<StreamSightInsights> {

    /** The query that fetches the insights from the RAINBOW storage. */
    protected query: TimeInstantQuery<Record<string, number>>;

    protected metricType = StreamSightInsightsMetric.instance;

    /** Total number times that this source has tried to query the composed metric. */
    private totalValueRequests = 0;

    /** Used to create/update the StreamSight insight topology for our metric. */
    private insightTopologyManager: InsightTopologyManager;

    /**
     * Stores the multicast value stream containing the metrics.
     *
     * We multicast the value stream to prevent duplicate requests, in case an interval elapses before the previous request has returned.
     */
    private valueStream: Observable<Sample<StreamSightInsights>>

    constructor(
        protected metricParams: StreamSightInsightsParams,
        metricsSource: MetricsSource,
        orchestrator: OrchestratorGateway,
        protected streamSightConfig: PolarisStreamSightConfig,
    ) {
        super(metricsSource, orchestrator);
        this.insightTopologyManager = new InsightTopologyManager(streamSightConfig);
        const timeSeriesSource = metricsSource.getTimeSeriesSource(RainbowStorageTimeSeriesSource.fullName);
        this.query = this.createQuery(timeSeriesSource);
    }

    getValueStream(): Observable<Sample<StreamSightInsights>> {
        if (!this.valueStream) {
            this.valueStream = this.createValueStream();
        }
        return this.valueStream;
    }

    private createValueStream(): Observable<Sample<StreamSightInsights>> {
        let requestPending = false;

        return this.getDefaultPollingInterval().pipe(
            // We want to ensure that we don't start a new request, while an old one is still pending.
            filter(() => !requestPending),
            tap(() => requestPending = true),

            switchMap(() => this.maintainInsightTopology()),
            switchMap(() => this.query.execute()),

            tap(() => requestPending = false),
            finalize(() => requestPending = false),

            map(result => this.assembleComposedMetric(result.results)?.samples[0]),
            share(),
        );
    }

    /**
     * Ensures the existence of the StreamSight insight topology for this composed metric on the first request and
     * subsequently on every nth request (e.g., on every 10th request).
     *
     * @returns An promise that resolves when the insight topology has been read/created/updated or if
     * this execution does not require a check.
     */
    private async maintainInsightTopology(): Promise<void> {
        // ToDo: Check if topology exists with a GET request, before creating a new one,
        // because if we simply recreate the topology we are always starting a new Java process on the analytics part.
        // if (this.totalValueRequests++ % CHECK_INSIGHT_TOPOLOGY_INTERVAL === 0) {
        if (this.totalValueRequests === 0) {
            // We intentionally do not convert the promise to an observable here, because we do not want
            // the increment of totalValueRequests to be skipped if the observable is cancelled.
            await this.insightTopologyManager.ensureInsightTopologyExists(this.metricParams);
            ++this.totalValueRequests;
        }
    }

    private createQuery(timeSeriesSource: TimeSeriesSource): TimeInstantQuery<Record<string, number>> {
        const metricName = this.insightTopologyManager.getInsightTopologyName(this.metricParams);
        let query = timeSeriesSource.select<Record<string, number>>('', metricName);

        // Create a filter statement for each insight.
        // Normally filters are combined with the AND operator, but we handle it differently for the RAINBOW storage.
        const keys = Object.keys(this.metricParams.insights);
        keys.forEach(key => {
            query = query.filterOnLabel(LabelFilters.equal('key', key));
        });

        return query;
    }

    private assembleComposedMetric(results: TimeSeriesInstant<Record<string, number>>[]): TimeSeriesInstant<StreamSightInsights> {
        if (results?.length === 0) {
            return undefined;
        }
        const result = results[0];

        return {
            dataType: DataType.Object,
            metricName: this.metricType.metricTypeName,
            start: result.start,
            end: result.end,
            labels: {},
            samples: result.samples,
        };
    }

}
