import {
    FilterOnLabelQueryContent,
    LabelComparisonOperator,
    NativeQueryBuilderBase,
    QueryContentType,
    QueryError,
    TimeSeriesQuery,
    TimeSeriesQueryResultType,
} from '@polaris-sloc/core';
import { PolarisStreamSightConfig } from '../../config';
import { GetAnalyticsRequest } from '../../model';
import { RainbowStorageNativeQuery } from './rainbow-storage-native-query';

interface RainbowStorageQueryInfo {
    query: GetAnalyticsRequest;
    metricName: string;
}

/**
 * Builds queries for the RAINBOW Distributed Data Storage.
 *
 * Important: This can currently only handle select statements.
 */
export class RainbowStorageNativeQueryBuilder extends NativeQueryBuilderBase  {

    constructor(private config: PolarisStreamSightConfig) {
        super();
    }

    buildQuery(resultType: TimeSeriesQueryResultType): TimeSeriesQuery<any> {
        const query = this.buildRainbowStorageQuery();
        return new RainbowStorageNativeQuery(this.config, resultType, query.metricName, query.query);
    }

    private buildRainbowStorageQuery(): RainbowStorageQueryInfo {
        const metricName = `${this.selectSegment.appName}_${this.selectSegment.metricName}`;
        return {
            metricName,
            query: {
                key: this.extractInsightKeys(),
            },
        };
    }

    private extractInsightKeys(): string[] {
        const keys: string[] = [];
        this.queryChainAfterSelect.forEach(segment => {
            if (segment.contentType === QueryContentType.FilterOnLabel) {
                const content = segment as FilterOnLabelQueryContent;
                if (content.filter.label === 'key' && content.filter.operator === LabelComparisonOperator.Equal) {
                    keys.push(content.filter.comparisonValue);
                }
            } else {
                throw new QueryError('RainbowStorageNativeQueryBuilder can currently only handle select and filterOnLabel statements.', this);
            }
        });
        return keys;
    }

}
