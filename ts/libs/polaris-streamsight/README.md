# @rainbow-h2020/polaris-streamsight

This library is the [StreamSight](https://gitlab.com/rainbow-project1/rainbow-analytics) query backend for the [Polaris SLO Cloud](https://polaris-slo-cloud.github.io) framework.

## Running unit tests

Run `nx test polaris-streamsight` to execute the unit tests via [Jest](https://jestjs.io).
