import {
    ApiObjectMetadata,
    ElasticityStrategy,
    ElasticityStrategyExecutionError,
    Logger,
    ObjectKind,
    OrchestratorClient,
    PodSpec,
    PodTemplateContainer,
    PolarisRuntime,
    SloCompliance,
    SloComplianceElasticityStrategyControllerBase,
    SloTarget,
} from '@polaris-sloc/core';
import {
    AdvancedStabilizationWindowTracker,
    K8sAffinityConfiguration,
    MigrationElasticityStrategy,
    MigrationElasticityStrategyConfig,
} from '@rainbow-h2020/common-mappings';
import { isEqual as _isEqual } from 'lodash';

/** Tracked executions eviction interval of 20 minutes. */
const EVICTION_INTERVAL_MSEC = 20 * 60 * 1000;

class K8sPodSpec extends PodSpec {
    affinity?: K8sAffinityConfiguration;
}

interface AffinityUpdateInfo {
    newAffinity: K8sAffinityConfiguration;
    isOutsideStabilizationWindow: boolean;
}

/**
 * Controller for the MigrationElasticityStrategy.
 */
export class MigrationElasticityStrategyController extends SloComplianceElasticityStrategyControllerBase<SloTarget, MigrationElasticityStrategyConfig> {
    /** The client for accessing orchestrator resources. */
    private orchClient: OrchestratorClient;

    /** Tracks the stabilization windows of the ElasticityStrategy instances. */
    private stabilizationWindowTracker: AdvancedStabilizationWindowTracker<MigrationElasticityStrategy, K8sAffinityConfiguration> =
        new AdvancedStabilizationWindowTracker();

    private evictionInterval: NodeJS.Timeout;

    constructor(polarisRuntime: PolarisRuntime) {
        super();
        this.orchClient = polarisRuntime.createOrchestratorClient();

        this.evictionInterval = setInterval(() => this.stabilizationWindowTracker.evictExpiredExecutions(), EVICTION_INTERVAL_MSEC);
    }

    checkIfActionNeeded(elasticityStrategy: ElasticityStrategy<SloCompliance, SloTarget, MigrationElasticityStrategyConfig>): Promise<boolean> {
        const lastExecution = this.stabilizationWindowTracker.getLastExecution(elasticityStrategy);
        if (!lastExecution) {
            return Promise.resolve(true);
        }

        // Check if the elasticity operation matches the newAffinity, so that we can skip loading the pod.
        // In theory, it is possible that the pod's affinity was modified externally. In such a case we would
        // need to wait for the last execution to be evicted from the stabilization window tracker to proceed past this point.
        const affinityUpdate = this.computeNewAffinity(elasticityStrategy);
        if (_isEqual(lastExecution.operation, affinityUpdate.newAffinity)) {
            return Promise.resolve(false);
        }

        if (!affinityUpdate.isOutsideStabilizationWindow) {
            Logger.log(
                'Skipping scaling, because stabilization window has not yet passed for: ',
                elasticityStrategy,
            );
            return Promise.resolve(false);
        }

        return Promise.resolve(true);
    }

    async execute(elasticityStrategy: MigrationElasticityStrategy): Promise<void> {
        Logger.log('Executing elasticity strategy:', elasticityStrategy);
        const affinityUpdate = this.computeNewAffinity(elasticityStrategy);
        const target = await this.loadTarget(elasticityStrategy);
        const podSpec = target.spec.template.spec as K8sPodSpec;

        if (_isEqual(podSpec.affinity, affinityUpdate.newAffinity)) {
            Logger.log(
                'Skipping scaling, because existing affinity equals new affinity for: ',
                elasticityStrategy,
            );
            return;
        }

        podSpec.affinity = affinityUpdate.newAffinity;
        await this.orchClient.update(target);
        this.stabilizationWindowTracker.trackExecution(elasticityStrategy, affinityUpdate.newAffinity);
        Logger.log('Successfully scaled.', elasticityStrategy, JSON.stringify(podSpec.affinity, null, '  '));
    }

    onDestroy(): void {
        clearInterval(this.evictionInterval);
    }

    onElasticityStrategyDeleted(elasticityStrategy: MigrationElasticityStrategy): void {
        this.stabilizationWindowTracker.removeElasticityStrategy(elasticityStrategy);
    }

    private computeNewAffinity(elasticityStrategy: MigrationElasticityStrategy): AffinityUpdateInfo {
        let isOutsideStabilizationWindow: boolean;
        let newAffinity: K8sAffinityConfiguration;

        // At or below 100 we use the baseNodeAffinity,
        // above 100 we use the alternativeNodeAffinity.
        // The tolerance is disregarded by this elasticity strategy.
        if (elasticityStrategy.spec.sloOutputParams.currSloCompliancePercentage <= 100) {
            newAffinity = elasticityStrategy.spec.staticConfig?.baseAffinity;
            isOutsideStabilizationWindow = this.stabilizationWindowTracker.isOutsideStabilizationWindowForScaleDown(elasticityStrategy)
        } else {
            newAffinity = elasticityStrategy.spec.staticConfig?.alternativeAffinity;
            isOutsideStabilizationWindow = this.stabilizationWindowTracker.isOutsideStabilizationWindowForScaleUp(elasticityStrategy);
        }

        return {
            newAffinity,
            isOutsideStabilizationWindow,
        }
    }

    private async loadTarget(elasticityStrategy: ElasticityStrategy<SloCompliance, SloTarget, MigrationElasticityStrategyConfig>): Promise<PodTemplateContainer> {
        const targetRef = elasticityStrategy.spec.targetRef;
        const queryApiObj = new PodTemplateContainer({
            objectKind: new ObjectKind({
                group: targetRef.group,
                version: targetRef.version,
                kind: targetRef.kind,
            }),
            metadata: new ApiObjectMetadata({
                namespace: elasticityStrategy.metadata.namespace,
                name: targetRef.name,
            }),
        });

        const ret = await this.orchClient.read(queryApiObj);
        if (!ret.spec?.template) {
            throw new ElasticityStrategyExecutionError('The SloTarget does not contain a pod template (spec.template field).', elasticityStrategy);
        }
        return ret;
    }
}
