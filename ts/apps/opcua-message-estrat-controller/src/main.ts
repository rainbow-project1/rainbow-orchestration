import { KubeConfig } from '@kubernetes/client-node';
import { OpcuaMessageElasticityStrategyKind, initPolarisLib as initMappingsLib } from '@rainbow-h2020/common-mappings';
import { Logger } from '@polaris-sloc/core';
import { initPolarisKubernetes } from '@polaris-sloc/kubernetes';
import { OpcuaMessageElasticityStrategyController } from './app/elasticity';

// Load the KubeConfig and initialize the @polaris-sloc/kubernetes library.
const k8sConfig = new KubeConfig();
k8sConfig.loadFromDefault();
// Really dirty hack to get around ERR_TLS_CERT_ALTNAME_INVALID, which occurs in hostNetwork mode.
k8sConfig.clusters = k8sConfig.clusters?.map(cluster => ({
    ...cluster,
    skipTLSVerify: true,
}));
const polarisRuntime = initPolarisKubernetes(k8sConfig);

// Initialize the used Polaris mapping libraries
initMappingsLib(polarisRuntime);

// Create an ElasticityStrategyManager and watch the supported elasticity strategy kinds.
const manager = polarisRuntime.createElasticityStrategyManager();
manager
    .startWatching({
        kindsToWatch: [{ kind: new OpcuaMessageElasticityStrategyKind(), controller: new OpcuaMessageElasticityStrategyController(polarisRuntime) }],
    })
    .catch(error => {
        Logger.error(error);
        process.exit(1);
    });
