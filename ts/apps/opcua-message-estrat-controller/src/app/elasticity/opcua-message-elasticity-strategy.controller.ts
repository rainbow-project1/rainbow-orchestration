/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import {
    ElasticityStrategy,
    Logger,
    OrchestratorClient,
    PolarisRuntime,
    SloCompliance,
    SloComplianceElasticityStrategyControllerBase,
    SloTarget,
} from '@polaris-sloc/core';
import {
    AdvancedStabilizationWindowTracker,
    OpcuaMessageElasticityStrategy,
    OpcuaMessageElasticityStrategyConfig,
} from '@rainbow-h2020/common-mappings';
import {
    AttributeIds,
    ClientSession,
    DataType,
    MessageSecurityMode,
    OPCUAClient,
    SecurityPolicy,
    WriteValueOptions,
} from 'node-opcua';

/** Tracked executions eviction interval of 20 minutes. */
const EVICTION_INTERVAL_MSEC = 20 * 60 * 1000;

interface OpcuaMessage {
    msgValue?: number;
    isOutsideStabilizationWindow: boolean;
}

/**
 * Controller for the OpcuaMessageElasticityStrategy.
 */
export class OpcuaMessageElasticityStrategyController extends SloComplianceElasticityStrategyControllerBase<SloTarget, OpcuaMessageElasticityStrategyConfig> {
    /** The client for accessing orchestrator resources. */
    private orchClient: OrchestratorClient;

    /** Tracks the stabilization windows of the ElasticityStrategy instances. */
    private stabilizationWindowTracker: AdvancedStabilizationWindowTracker<OpcuaMessageElasticityStrategy, OpcuaMessage> =
        new AdvancedStabilizationWindowTracker();

    private evictionInterval: NodeJS.Timeout;

    constructor(polarisRuntime: PolarisRuntime) {
        super();
        this.orchClient = polarisRuntime.createOrchestratorClient();

        this.evictionInterval = setInterval(() => this.stabilizationWindowTracker.evictExpiredExecutions(), EVICTION_INTERVAL_MSEC);
    }

    checkIfActionNeeded(elasticityStrategy: ElasticityStrategy<SloCompliance, SloTarget, OpcuaMessageElasticityStrategyConfig>): Promise<boolean> {
        const lastExecution = this.stabilizationWindowTracker.getLastExecution(elasticityStrategy);
        if (!lastExecution) {
            return Promise.resolve(true);
        }

        const msg = this.computeOpcuaMessage(elasticityStrategy);
        if (msg.msgValue === lastExecution.operation.msgValue) {
            return Promise.resolve(false);
        }

        if (!msg.isOutsideStabilizationWindow) {
            Logger.log(
                'Skipping scaling, because stabilization window has not yet passed for: ',
                elasticityStrategy,
            );
            return Promise.resolve(false);
        }

        return Promise.resolve(true);
    }

    async execute(elasticityStrategy: OpcuaMessageElasticityStrategy): Promise<void> {
        Logger.log('Executing elasticity strategy:', elasticityStrategy);
        const msg = this.computeOpcuaMessage(elasticityStrategy);
        if (msg.msgValue === undefined || msg.msgValue === null) {
            Logger.log('No message configured for the current SLO state, skipping elasticity strategy execution.');
            return;
        }

        const opcuaSession = await this.connectToOpcuaServer(elasticityStrategy.spec.staticConfig);
        const writeOptions: WriteValueOptions = {
            nodeId: elasticityStrategy.spec.staticConfig.nodeName,
            attributeId: AttributeIds.Value,
            value: {
                value: {
                    dataType: DataType.Float,
                    value: msg.msgValue,
                },
            },
        };

        const dataValue =  await opcuaSession.write(writeOptions)
        Logger.log('Successfully wrote data value', dataValue)
    }

    onDestroy(): void {
        clearInterval(this.evictionInterval);
    }

    onElasticityStrategyDeleted(elasticityStrategy: OpcuaMessageElasticityStrategy): void {
        this.stabilizationWindowTracker.removeElasticityStrategy(elasticityStrategy);
    }

    private computeOpcuaMessage(elasticityStrategy: OpcuaMessageElasticityStrategy): OpcuaMessage {
        let isOutsideStabilizationWindow: boolean;
        let msgValue: number;

        // At or below 100 we use the baseValue,
        // above 100 we use the alternateValue.
        // The tolerance is disregarded by this elasticity strategy.
        if (elasticityStrategy.spec.sloOutputParams.currSloCompliancePercentage <= 100) {
            msgValue = elasticityStrategy.spec.staticConfig?.baseValue;
            isOutsideStabilizationWindow = this.stabilizationWindowTracker.isOutsideStabilizationWindowForScaleDown(elasticityStrategy)
        } else {
            msgValue = elasticityStrategy.spec.staticConfig?.alternateValue;
            isOutsideStabilizationWindow = this.stabilizationWindowTracker.isOutsideStabilizationWindowForScaleUp(elasticityStrategy);
        }

        return {
            msgValue,
            isOutsideStabilizationWindow,
        }
    }

    private async connectToOpcuaServer(config: OpcuaMessageElasticityStrategyConfig): Promise<ClientSession> {
        const connectionStrategy = {
            initialDelay: 1000,
            maxRetry: 1,
        }
        const options = {
            applicationName: 'OpcuaMessageElasticityStrategy',
            connectionStrategy,
            securityMode: MessageSecurityMode.None,
            securityPolicy: SecurityPolicy.None,
            // eslint-disable-next-line @typescript-eslint/naming-convention
            endpoint_must_exist: false,
        };

        const client = OPCUAClient.create(options);
        await client.connect(config.serverUrl);
        return client.createSession();
    }
}
