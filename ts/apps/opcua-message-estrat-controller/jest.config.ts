/* eslint-disable */
export default {
    displayName: 'opcua-message-estrat-controller',
    preset: '../../jest.preset.js',
    globals: {
        'ts-jest': {
            tsconfig: '<rootDir>/tsconfig.spec.json',
        },
    },
    testEnvironment: 'node',
    transform: {
        '^.+\\.[tj]s$': 'ts-jest',
    },
    moduleFileExtensions: ['ts', 'js', 'html'],
    coverageDirectory: '../../coverage/apps/opcua-message-estrat-controller',
    reporters: [
        'default',
        [
            'jest-junit',
            {
                outputDirectory: './junit-reports',
                outputName: 'opcua-message-estrat-controller.xml',
            },
        ],
    ],
};
